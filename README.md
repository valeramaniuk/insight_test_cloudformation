###CLOUDFORMATION template creates:
1. RedShift Cluster 
1. S3 bucket with a 'private' ACL
1. IAM role to allow the cluster to access this bucket

### Input parameters:

* Cluster type 
* Cluster size
* The cluster name
    * will be used as a prefix for the bucket name, roles and policies 
* Charge Code tag to be applied to all resources
    * cluster/ bucket only since they are the only 'paid' resources
####Additionally:
* Name of the database for the cluster
* Name of master user for your cluster
* Master password for the database
###Output:
* ARN of the S3 bucket
* Name of the database for the cluster
####Additionally:
* URL of the S3 bucket
* RShift database name
* RShift database Master User name

###Mics
Tested and works
Developed as a part of Test Code for a staffing agency 9/24/2017
Valera Maniuk valeramaniuk@protonmail.com


